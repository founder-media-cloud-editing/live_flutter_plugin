///
/// Created by aby$ on 12/22/21$.
///

import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:live_flutter_plugin/manager/tx_audio_effect_manager.dart';
import 'package:live_flutter_plugin/manager/tx_beauty_manager.dart';
import 'package:live_flutter_plugin/manager/tx_device_manager.dart';

import 'package:uuid/uuid.dart';

import 'v2_tx_live_code.dart';
import 'v2_tx_live_def.dart';
import 'manager/tx_audio_effect_manager.dart';
import 'manager/tx_beauty_manager.dart';
import 'manager/tx_device_manager.dart';
import 'manager/tx_live_manager.dart';
import 'v2_tx_live_pusher_observer.dart';

class V2TXLivePusher {
  int status = 0;

  late MethodChannel _channel;
  late String _identifier;
  late Set<V2TXLivePusherObserver> _listeners;

  late TXAudioEffectManager _audioEffectManager;
  late TXBeautyManager _beautyManager;
  late TXDeviceManager _deviceManager;

  V2TXLivePusher(V2TXLiveMode liveMode) {
    _identifier = const Uuid().v1();
    _listeners = {};
    _initMethodChannel(_identifier);
    _audioEffectManager = TXAudioEffectManager(_channel);
    _beautyManager = TXBeautyManager(_channel);
    _deviceManager = TXDeviceManager(_channel);
    _createNativePusher(_identifier, liveMode);
  }

  /// MethodChannel初始化
  _initMethodChannel(String identifier) {
    // init Method Channel
    _channel = MethodChannel("pusher_$_identifier");
    // methodChannel CallHandler
    _channel.setMethodCallHandler((MethodCall call) async {
      debugPrint("MethodCallHandler method: ${call.method}");
      var arguments = call.arguments;
      if (arguments is Map) {
        debugPrint("arguments is Map: $arguments");
      } else {
        debugPrint("arguments isn't Map: $arguments");
      }
      if (call.method == "onPusherListener") {
        var typeStr = arguments['type'];
        var params = arguments['params'];
        debugPrint("on Pusher Listener: type: $typeStr");

        V2TXLivePusherListenerType? callType;
        for (var subType in V2TXLivePusherListenerType.values) {
          if (subType.toString().replaceFirst("V2TXLivePusherListenerType.", "") == typeStr) {
            callType = subType;
            break;
          }
        }
        if (callType != null) {
          _doListenerCallBack(callType, params);
        }
      } else {
        debugPrint("on Pusher Listener: MethodNotImplemented ${call.method}");
      }
    });
  }

  /// Pusher 回调
  void _doListenerCallBack(V2TXLivePusherListenerType type, params) {
    for (var item in _listeners) {
      item(type, params);
    }
  }

  /// 添加推流器回调。
  ///
  /// 通过设置回调，可以监听 V2TXLivePusher 推流器的一些回调事件，
  /// 包括推流器状态、音量回调、统计数据、警告和错误信息等。
  ///
  /// @param V2TXLivePusherObserver 播放器的回调目标对象，更多信息请查看 {@link V2TXLivePlayerObserver}
  void addListener(V2TXLivePusherObserver func) {
    _listeners.add(func);
  }

  /// 移除推流器回调。
  void removeListener(V2TXLivePusherObserver func) {
    _listeners.remove(func);
  }

  void destroy() {
    _listeners.removeAll;
    TXLiveManager.destroyPusher(_identifier);
  }

  void _createNativePusher(String identifier, V2TXLiveMode mode) async {
    int result;
    try {
      result = await TXLiveManager.createPusher(identifier, mode);
    } on PlatformException {
      result = -1;
    }
    debugPrint("create pusher result $result identifier $identifier");
    status = result;
  }

  /// V2TXLiveCode返回值处理
  V2TXLiveCode _liveCodeWithResult(result) {
    return V2TXLiveFlutterResult.v2TXLiveCode(result) ?? V2TXLIVE_ERROR_FAILED;
  }

  /// 设置本地摄像头预览视图的ID
  ///
  /// 本地摄像头采集到的画面，经过美颜、脸形调整、滤镜等多种效果叠加之后，最终会显示到传入的 View 上。
  /// @param identifier 本地摄像头预览视图id
  /// @return 返回值 {@link V2TXLiveCode}
  ///         - V2TXLIVE_OK：成功
  Future<V2TXLiveCode> setRenderViewID(int viewID) async {
    var result = await _channel.invokeMethod("setRenderView", {"id": viewID});
    return _liveCodeWithResult(result);
  }

  /// 摄像头镜像类型
  ///
  /// @param mirrorType V2TXLiveMirrorType
  /// - V2TXLiveMirrorTypeAuto  【默认值】: 默认镜像类型. 在这种情况下，前置摄像头的画面是镜像的，后置摄像头的画面不是镜像的
  /// - V2TXLiveMirrorTypeEnable:  前置摄像头 和 后置摄像头，都切换为镜像模式
  /// - V2TXLiveMirrorTypeDisable: 前置摄像头 和 后置摄像头，都切换为非镜像模式
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setRenderMirror(V2TXLiveMirrorType mirrorType) async {
    var result = await _channel.invokeMethod(
        'setRenderMirror', {"mirrorType": mirrorType.index});
    return _liveCodeWithResult(result);
  }


  ///
  /// 设置视频编码镜像。
  ///
  /// 编码镜像只影响观众端看到的视频效果。
  /// mirror 是否镜像
  /// - false【默认值】: 播放端看到的是非镜像画面
  /// - true: 播放端看到的是镜像画面
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setEncoderMirror(bool mirror) async {
    var result = await _channel.invokeMethod(
        'setEncoderMirror', {"mirror": mirror});
    return _liveCodeWithResult(result);
  }

  ///
  /// 设置本地摄像头预览画面的旋转角度。
  ///
  /// 只旋转本地预览画面，不影响推流出去的画面。
  /// rotation 预览画面的旋转角度 {@link V2TXLiveDef.V2TXLiveRotation}
  /// - V2TXLiveRotation0【默认值】: 0度, 不旋转
  /// - V2TXLiveRotation90:  顺时针旋转90度
  /// - V2TXLiveRotation180: 顺时针旋转180度
  /// - V2TXLiveRotation270: 顺时针旋转270度
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setRenderRotation(V2TXLiveRotation rotation) async {
    var result = await _channel.invokeMethod(
        'setRenderRotation', {"rotation": rotation.index});
    return _liveCodeWithResult(result);
  }

  ///
  /// 打开本地摄像头。
  ///
  /// frontCamera 指定摄像头方向是否为前置
  /// - true 【默认值】: 切换到前置摄像头
  /// - false: 切换到后置摄像头
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// @note startVirtualCamera，startCamera，startScreenCapture，同一 Pusher 实例下，仅有一个能上行，三者为覆盖关系。例如先调用 startCamera，后调用 startVirtualCamera。此时表现为暂停摄像头推流，开启图片推流
  Future<V2TXLiveCode> startCamera(bool frontCamera) async {
    var result = await _channel.invokeMethod(
        'startCamera', {"frontCamera": frontCamera});
    return _liveCodeWithResult(result);
  }

  /// 关闭本地摄像头。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  ///
  Future<void> stopCamera() async {
    return await _channel.invokeMethod('stopCamera', {});
  }

  ///
  /// 打开麦克风。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> startMicrophone() async {
    var result = await _channel.invokeMethod('startMicrophone', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 关闭麦克风。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  ///
  Future<V2TXLiveCode> stopMicrophone() async {
    var result = await _channel.invokeMethod('stopMicrophone', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 开启图片推流。
  ///
  /// type:network, file
  /// imageUrl:图片地址
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// @note startVirtualCamera，startCamera，startScreenCapture，同一 Pusher 实例下，仅有一个能上行，三者为覆盖关系。例如先调用 startCamera，后调用 startVirtualCamera。此时表现为暂停摄像头推流，开启图片推流
  Future<V2TXLiveCode> startVirtualCamera(String type, String imageUrl) async {
    var result = await _channel.invokeMethod(
        'startVirtualCamera',
        {"type": type, "imageUrl": imageUrl});
    return _liveCodeWithResult(result);
  }

  ///
  /// 关闭图片推流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> stopVirtualCamera() async {
    var result = await _channel.invokeMethod('stopVirtualCamera', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 开启屏幕采集。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// @note startVirtualCamera，startCamera，startScreenCapture，同一 Pusher 实例下，仅有一个能上行，三者为覆盖关系。例如先调用 startCamera，后调用 startVirtualCamera。此时表现为暂停摄像头推流，开启图片推流
  Future<V2TXLiveCode> startScreenCapture(String appGroup) async {
    var result = await _channel.invokeMethod('startScreenCapture', {"appGroup": appGroup});
    return _liveCodeWithResult(result);
  }

  ///
  /// 关闭屏幕采集。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> stopScreenCapture() async {
    var result = await _channel.invokeMethod('stopScreenCapture', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 暂停推流器的音频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> pauseAudio() async {
    var result = await _channel.invokeMethod('pauseAudio', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 恢复推流器的音频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> resumeAudio() async {
    var result = await _channel.invokeMethod('resumeAudio', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 暂停推流器的视频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> pauseVideo() async {
    var result = await _channel.invokeMethod('pauseVideo', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 恢复推流器的视频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> resumeVideo() async {
    var result = await _channel.invokeMethod('resumeVideo', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 开始音视频数据推流。
  ///
  /// url 推流的目标地址，支持任意推流服务端
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 操作成功，开始连接推流目标地址
  /// - V2TXLIVE_ERROR_INVALID_PARAMETER: 操作失败，url 不合法
  /// - V2TXLIVE_ERROR_INVALID_LICENSE: 操作失败，license 不合法，鉴权失败
  /// - V2TXLIVE_ERROR_REFUSED: 操作失败，RTC 不支持同一设备上同时推拉同一个 StreamId
  ///
  Future<V2TXLiveCode> startPush(String url) async {
    var result = await _channel.invokeMethod('startPush', {"url": url});
    return _liveCodeWithResult(result);
  }

  ///
  /// 停止推送音视频数据。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> stopPush() async {
    var result = await _channel.invokeMethod('stopPush', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 当前推流器是否正在推流中。
  ///
  /// @return 是否正在推流
  /// - 1: 正在推流中
  /// - 0: 已经停止推流
  Future<V2TXLiveCode> isPushing() async {
    var result = await _channel.invokeMethod('isPushing', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 设置推流音频质量。
  ///
  /// quality 音频质量 {@link V2TXLiveDef.V2TXLiveAudioQuality}
  /// - V2TXLiveAudioQualityDefault 【默认值】: 通用
  /// - V2TXLiveAudioQualitySpeech: 语音
  /// - V2TXLiveAudioQualityMusic:  音乐
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_REFUSED: 推流过程中，不允许调整音质
  Future<V2TXLiveCode> setAudioQuality(V2TXLiveAudioQuality quality) async {
    var result = await _channel.invokeMethod(
        'setAudioQuality', {"quality": quality.index});
    return _liveCodeWithResult(result);
  }

  ///
  /// 设置推流视频编码参数
  ///
  /// param 视频编码参数 {@link V2TXLiveDef.V2TXLiveVideoEncoderParam}
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setVideoQuality(V2TXLiveVideoEncoderParam param) async {
    var result = await _channel.invokeMethod(
        'setVideoQuality', param.toJson());
    return _liveCodeWithResult(result);
  }

  ///
  /// 获取音效管理对象 {@link TXAudioEffectManager}。
  ///
  /// 通过音效管理，您可以使用以下功能：
  /// - 调整麦克风收集的人声音量。
  /// - 设置混响和变声效果。
  /// - 开启耳返，设置耳返音量。
  /// - 添加背景音乐，调整背景音乐的播放效果。
  TXAudioEffectManager getAudioEffectManager() {
    _channel.invokeMethod('getAudioEffectManager');
    return _audioEffectManager;
  }

  ///
  /// 获取美颜管理对象 {@link TXBeautyManager}。
  ///
  /// 通过美颜管理，您可以使用以下功能：
  /// - 设置”美颜风格”、”美白”、“红润”、“大眼”、“瘦脸”、“V脸”、“下巴”、“短脸”、“小鼻”、“亮眼”、“白牙”、“祛眼袋”、“祛皱纹”、“祛法令纹”等美容效果。
  /// - 调整“发际线”、“眼间距”、“眼角”、“嘴形”、“鼻翼”、“鼻子位置”、“嘴唇厚度”、“脸型”。
  /// - 设置人脸挂件（素材）等动态效果。
  /// - 添加美妆。
  /// - 进行手势识别。
  TXBeautyManager getBeautyManager() {
    _channel.invokeMethod('getBeautyManager');
    return _beautyManager;
  }

  ///
  /// 获取设备管理对象 {@link TXDeviceManager}。
  ///
  /// 通过设备管理，您可以使用以下功能：
  /// - 切换前后摄像头。
  /// - 设置自动聚焦。
  /// - 设置摄像头缩放倍数。
  /// - 打开或关闭闪光灯。
  /// - 切换耳机或者扬声器。
  /// - 修改音量类型(媒体音量或者通话音量)。
  TXDeviceManager getDeviceManager() {
    _channel.invokeMethod('getDeviceManager');
    return _deviceManager;
  }

  ///
  /// 截取推流过程中的本地画面。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_REFUSED: 已经停止推流，不允许调用截图操作
  ///
  Future<V2TXLiveCode> snapshot() async {
    var result = await _channel.invokeMethod('snapshot', {});
    return _liveCodeWithResult(result);
  }

  ///
  /// 设置推流器水印。默认情况下，水印不开启。
  ///
  /// image 水印图片。如果该值为 null，则等效于禁用水印
  /// x     水印的横坐标，取值范围为0 - 1的浮点数。
  /// y     水印的纵坐标，取值范围为0 - 1的浮点数。
  /// scale 水印图片的缩放比例，取值范围为0 - 1的浮点数。
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  ///
  Future<V2TXLiveCode> setWatermark(String type, String image, double x,
      double y, double scale) async {
    var result = await _channel.invokeMethod('setWatermark', {
      "type": type,
      "image": image,
      "x": x.toString(),
      "y": y.toString(),
      "scale": scale.toString()
    });
    return _liveCodeWithResult(result);
  }

  ///
  /// 启用采集音量大小提示。
  /// <p>
  /// 开启后可以在 {@link V2TXLivePusherObserver#onMicrophoneVolumeUpdate(int)} 回调中获取到 SDK 对音量大小值的评估。
  ///
  /// intervalMs 决定了 onMicrophoneVolumeUpdate 回调的触发间隔，单位为ms，最小间隔为100ms，如果小于等于0则会关闭回调，建议设置为300ms；【默认值】：0，不开启
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  ///
  Future<V2TXLiveCode> enableVolumeEvaluation(int intervalMs) async {
    var result = await _channel.invokeMethod(
        'enableVolumeEvaluation', {"intervalMs": intervalMs,});
    return _liveCodeWithResult(result);
  }

  ///
  /// 开启/关闭自定义视频处理。
  ///
  /// enable true: 开启; false: 关闭。【默认值】: false
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_NOT_SUPPORTED: 不支持的格式
  /// @note RTMP 支持格式：
  /// format = V2TXLivePixelFormatTexture2D && buffetType = V2TXLiveBufferTypeTexture
  /// RTC 支持格式：
  /// format = V2TXLivePixelFormatTexture2D && bufferType = V2TXLiveBufferTypeTexture
  /// format = V2TXLivePixelFormatI420 && bufferType = V2TXLiveBufferTypeByteBuffer
  /// format = V2TXLivePixelFormatI420 && bufferType = V2TXLiveBufferTypeByteArray
  ///
  Future<V2TXLiveCode> enableCustomVideoProcess(bool enable,
      V2TXLivePixelFormat pixelFormat, V2TXLiveBufferType bufferType) async {
    var result = await _channel.invokeMethod('enableCustomVideoProcess', {
      "enable": enable,
      "pixelFormat": pixelFormat.index,
      "bufferType": bufferType.index,
    });
    return _liveCodeWithResult(result);
  }

  ///
  /// 开启/关闭自定义视频采集。
  /// <p>
  /// 在自定义视频采集模式下，SDK 不再从摄像头采集图像，只保留编码和发送能力。
  ///
  /// enable true：开启自定义采集；false：关闭自定义采集。【默认值】：false
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// @note 需要在 [startPush](@ref V2TXLivePusher#startPush:) 之前调用，才会生效。
  ///
  Future<V2TXLiveCode> enableCustomVideoCapture(bool enable) async {
    var result = await _channel.invokeMethod(
        'enableCustomVideoCapture', {"enable": enable});
    return _liveCodeWithResult(result);
  }

  ///
  /// 开启/关闭自定义音频采集
  ///
  /// enable true: 开启自定义采集; false: 关闭自定义采集。【默认值】: false
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// @brief 开启/关闭自定义音频采集。<br/>
  /// 在自定义音频采集模式下，SDK 不再从麦克风采集声音，只保留编码和发送能力。
  /// @note 需要在 [startPush]({@link V2TXLivePusher#startPush(String)}) 前调用才会生效。
  ///
  Future<V2TXLiveCode> enableCustomAudioCapture(bool enable) async {
    var result = await _channel.invokeMethod(
        'enableCustomAudioCapture', {"enable": enable});
    return _liveCodeWithResult(result);
  }

  ///
  /// 在自定义视频采集模式下，将采集的视频数据发送到SDK。
  /// <p>
  /// 在自定义视频采集模式下，SDK不再采集摄像头数据，仅保留编码和发送功能。
  ///
  /// @param videoFrame 向 SDK 发送的 视频帧数据 {@link V2TXLiveDef.V2TXLiveVideoFrame}
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_INVALID_PARAMETER: 发送失败，视频帧数据不合法
  /// - V2TXLIVE_ERROR_REFUSED: 发送失败，您必须先调用 enableCustomVideoCapture 开启自定义视频采集。
  /// @note 需要在 {@link V2TXLivePusher#startPush(String)} 之前调用 {@link V2TXLivePusher#enableCustomVideoCapture(boolean)} 开启自定义采集。
  ///
  Future<V2TXLiveCode> sendCustomVideoFrame(V2TXLiveVideoFrame videoFrame) async {
    var result = await _channel.invokeMethod("V2TXLiveAudioFrame", videoFrame.toJson());
    return _liveCodeWithResult(result);
  }

  ///
  /// 在自定义音频采集模式下，将采集的音频数据发送到SDK
  ///
  /// @param audioFrame 向 SDK 发送的 音频帧数据 {@link V2TXLiveDef.V2TXLiveAudioFrame}
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_REFUSED: 发送失败，您必须先调用 enableCustomAudioCapture 开启自定义音频采集
  /// @brief 在自定义音频采集模式下，将采集的音频数据发送到SDK，SDK不再采集麦克风数据，仅保留编码和发送功能。
  /// @note 需要在 [startPush]({@link V2TXLivePusher#startPush(String)}) 之前调用  {@link V2TXLivePusher#enableCustomAudioCapture(boolean)} 开启自定义采集。
  ///
  Future<V2TXLiveCode> sendCustomAudioFrame(V2TXLiveAudioFrame audioFrame) async {
    var result = await _channel.invokeMethod("V2TXLiveAudioFrame", audioFrame.toJson());
    return _liveCodeWithResult(result);
  }

  ///
  /// 发送 SEI 消息
  /// <p>
  /// 播放端 {@link V2TXLivePlayer} 通过 {@link V2TXLivePlayerObserver} 中的  `onReceiveSeiMessage` 回调来接收该消息。
  ///
  /// payloadType 数据类型，支持 5、242。推荐填：242
  /// data        待发送的数据
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  ///
  Future<V2TXLiveCode> sendSeiMessage(int payloadType, Uint8List data) async {
    var result = await _channel.invokeMethod('sendSeiMessage',
        {"payloadType": payloadType, "data": data});
    return _liveCodeWithResult(result);
  }

  ///
  /// 显示仪表盘。
  ///
  /// isShow 是否显示。【默认值】：false
  ///
  Future<V2TXLiveCode> showDebugView(bool isShow) async {
    var result = await _channel.invokeMethod(
        'showDebugView', {"isShow": isShow});
    return _liveCodeWithResult(result);
  }

  ///
  /// 调用 V2TXLivePusher 的高级 API 接口。
  ///
  /// @param key   高级 API 对应的 key。
  /// @param value 调用 key 所对应的高级 API 时，需要的参数 jsonString。
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_INVALID_PARAMETER: 操作失败，key 不允许为空
  /// @note 该接口用于调用一些高级功能。
  ///
  Future<V2TXLiveCode> setProperty(String key, String value) async {
    var result = await _channel.invokeMethod(
        'setProperty', {"key": key, "value": value});
    return _liveCodeWithResult(result);
  }

  ///
  /// 设置云端的混流转码参数
  /// <p>
  /// 如果您在实时音视频 [控制台](https://console.cloud.tencent.com/trtc/) 中的功能配置页开启了“启用旁路推流”功能，
  /// 房间里的每一路画面都会有一个默认的直播 [CDN 地址](https://cloud.tencent.com/document/product/647/16826)
  /// <p>
  /// 一个直播间中可能有不止一位主播，而且每个主播都有自己的画面和声音，但对于 CDN 观众来说，他们只需要一路直播流
  /// 所以您需要将多路音视频流混成一路标准的直播流，这就需要混流转码
  /// <p>
  /// 当您调用 setMixTranscodingConfig() 接口时，SDK 会向腾讯云的转码服务器发送一条指令，目的是将房间里的多路音视频流混合为一路,
  /// 您可以通过 mixStreams 参数来调整每一路画面的位置，以及是否只混合声音，也可以通过 videoWidth、videoHeight、videoBitrate 等参数控制混合音视频流的编码参数
  ///
  /// <pre>
  /// 【画面1】=> 解码 ====> \
  ///                         \
  /// 【画面2】=> 解码 =>  画面混合 => 编码 => 【混合后的画面】
  ///                         /
  /// 【画面3】=> 解码 ====> /
  ///
  /// 【声音1】=> 解码 ====> \
  ///                         \
  /// 【声音2】=> 解码 =>  声音混合 => 编码 => 【混合后的声音】
  ///                         /
  /// 【声音3】=> 解码 ====> /
  /// </pre>
  /// <p>
  /// 参考文档：[云端混流转码](https://cloud.tencent.com/document/product/647/16827)
  ///
  /// @param config 请参考 V2TXLiveDef.java 中关于 {@link V2TXLiveDef.V2TXLiveTranscodingConfig} 的介绍。如果传入 null 则取消云端混流转码
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_REFUSED: 未开启推流时，不允许设置混流转码参数
  /// @note 关于云端混流的注意事项：
  /// - 仅支持 RTC 模式混流
  /// - 云端转码会引入一定的 CDN 观看延时，大概会增加1 - 2秒
  /// - 调用该函数的用户，会将连麦中的多路画面混合到自己当前这路画面或者 config 中指定的 streamId 上
  /// - 请注意，若您还在房间中且不再需要混流，请务必传入 null 进行取消，因为当您发起混流后，云端混流模块就会开始工作，不及时取消混流可能会引起不必要的计费损失
  /// - 请放心，您退房时会自动取消混流状态

  Future<V2TXLiveCode> setMixTranscodingConfig(V2TXLiveTranscodingConfig? config) async {
    var result = await _channel.invokeMethod('setMixTranscodingConfig',
        {"config": config?.toJson() ?? {}});
    return _liveCodeWithResult(result);
  }
}

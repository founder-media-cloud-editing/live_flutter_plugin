///
/// Created by aby$ on 12/22/21$.
///

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';
import 'manager/tx_live_manager.dart';
import 'v2_tx_live_code.dart';
import 'v2_tx_live_def.dart';
import 'v2_tx_live_player_observer.dart';

class V2TXLivePlayer {
  int status = 0;
  late MethodChannel _channel;
  late String _identifier;
  late Set<V2TXLivePlayerObserver> _listeners;

  V2TXLivePlayer() {
    _identifier = const Uuid().v1();
    _listeners = {};
    _initMethodChannel(_identifier);
    _createNativePlayer(_identifier);
  }

  /// MethodChannel初始化
  _initMethodChannel(String identifier) {
    // init Method Channel
    _channel = MethodChannel("player_$identifier");
    // methodChannel CallHandler
    _channel.setMethodCallHandler((MethodCall call) async {
      debugPrint("MethodCallHandler method: ${call.method}");
      var arguments = call.arguments;
      if (arguments is Map) {
        debugPrint("arguments is Map: $arguments");
      } else {
        debugPrint("arguments isn't Map: $arguments");
      }
      if (call.method == "onPlayerListener") {
        String typeStr = arguments['type'];
        var params = arguments['params'];
        debugPrint("on Player Listener: type:$typeStr");

        V2TXLivePlayerListenerType? callType;
        for (var subType in V2TXLivePlayerListenerType.values) {
          if (subType.toString().replaceFirst("V2TXLivePlayerListenerType.", "") == typeStr) {
            callType = subType;
            break;
          }
        }
        if (callType != null) {
          _doListenerCallBack(callType, params);
        }
      } else {
        debugPrint("on Player Listener: MethodNotImplemented ${call.method}");
      }
    });
  }

  /// Player 回调
  void _doListenerCallBack(V2TXLivePlayerListenerType type, params) {
    for (var item in _listeners) {
      item(type, params);
    }
  }

  /// 添加播放器回调。
  ///
  /// 通过设置回调，可以监听 V2TXLivePlayer 播放器的一些回调事件，
  /// 包括播放器状态、播放音量回调、音视频首帧回调、统计数据、警告和错误信息等。
  ///
  /// @param V2TXLivePlayerObserver 播放器的回调目标对象，更多信息请查看 {@link V2TXLivePlayerObserver}
  void addListener(V2TXLivePlayerObserver func) {
    _listeners.add(func);
  }

  /// 移除播放器回调。
  void removeListener(V2TXLivePlayerObserver func) {
    _listeners.remove(func);
  }

  void destroy() {
    _listeners.removeAll;
    TXLiveManager.destroyPlayer(_identifier);
  }

  void _createNativePlayer(String identifier) async {
    int result;
    try {
      result = await TXLiveManager.createPlayer(identifier);
    } on PlatformException {
      result = -1;
    }
    // debugPrint("create pusher result $result identifier $identifier");
    status = result;
  }

  /// V2TXLiveCode 错误码返回值处理
  V2TXLiveCode _liveCodeWithResult<T>(T? result) {
    return V2TXLiveFlutterResult.v2TXLiveCode(result) ?? V2TXLIVE_ERROR_FAILED;
  }

  /// 设置本地摄像头预览视图的ID
  /// 本地摄像头采集到的画面，经过美颜、脸形调整、滤镜等多种效果叠加之后，最终会显示到传入的 View 上。
  /// @param identifier 本地摄像头预览视图id
  /// @return 返回值 {@link V2TXLiveCode}
  ///         - V2TXLIVE_OK：成功
  Future<V2TXLiveCode> setRenderViewID(int viewID) async {
    var result = await _channel.invokeMethod("setRenderView", {"id": viewID});
    return _liveCodeWithResult(result);
  }

  /// 摄像头镜像类型
  /// rotation 旋转角度 {@link V2TXLiveDef.V2TXLiveRotation}
  /// - V2TXLiveRotation0【默认值】: 0度, 不旋转
  /// - V2TXLiveRotation90:  顺时针旋转90度
  /// - V2TXLiveRotation180: 顺时针旋转180度
  /// - V2TXLiveRotation270: 顺时针旋转270度
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setRenderRotation(V2TXLiveRotation rotation) async {
    var result = await _channel.invokeMethod(
        'setRenderRotation', {"rotation": rotation.index});
    return _liveCodeWithResult(result);
  }

  /// 设置画面的填充模式。
  /// mode 画面填充模式 {@link V2TXLiveDef.V2TXLiveFillMode}。
  /// - V2TXLiveFillModeFill 【默认值】: 图像铺满屏幕，不留黑边，如果图像宽高比不同于屏幕宽高比，部分画面内容会被裁剪掉
  /// - V2TXLiveFillModeFit: 图像适应屏幕，保持画面完整，但如果图像宽高比不同于屏幕宽高比，会有黑边的存在
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setRenderFillMode(V2TXLiveFillMode mode) async {
    var result = await _channel.invokeMethod('setRenderFillMode', {"mode": mode.index});
    return _liveCodeWithResult(result);
  }

  /// 开始播放音视频流。
  /// url 音视频流的播放地址，支持 RTMP, HTTP-FLV, TRTC。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> startPlay(String url) async {
    var result = await _channel.invokeMethod('startPlay', {"url": url});
    return _liveCodeWithResult(result);
  }

  /// 停止播放音视频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> stopPlay() async {
    var result = await _channel.invokeMethod('stopPlay', {});
    return _liveCodeWithResult(result);
  }

  /// 播放器是否正在播放中。
  ///
  /// @return 是否正在播放
  /// - 1: 正在播放中
  /// - 0: 已经停止播放
  Future<V2TXLiveCode> isPlaying() async {
    var result = await _channel.invokeMethod('isPlaying', {});
    return _liveCodeWithResult(result);
  }

  /// 暂停播放器的音频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> pauseAudio() async {
    var result = await _channel.invokeMethod('pauseAudio', {});
    return _liveCodeWithResult(result);
  }

  /// 恢复播放器的音频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> resumeAudio() async {
    var result = await _channel.invokeMethod('resumeAudio', {});
    return _liveCodeWithResult(result);
  }

  /// 暂停播放器的视频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> pauseVideo() async {
    var result = await _channel.invokeMethod('pauseVideo', {});
    return _liveCodeWithResult(result);
  }

  /// 恢复播放器的视频流。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> resumeVideo() async {
    var result = await _channel.invokeMethod('resumeVideo', {});
    return _liveCodeWithResult(result);
  }

  /// 设置播放器音量。
  ///
  /// volume 音量大小，取值范围0 - 100。【默认值】: 100
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> setPlayoutVolume(int volume) async {
    var result = await _channel.invokeMethod('setPlayoutVolume', {"volume": volume});
    return _liveCodeWithResult(result);
  }

  /// 设置播放器缓存自动调整的最小和最大时间 ( 单位：秒 )。
  ///
  /// minTime 缓存自动调整的最小时间，取值需要大于0。【默认值】：1
  /// maxTime 缓存自动调整的最大时间，取值需要大于0。【默认值】：5
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_INVALID_PARAMETER: 操作失败，minTime 和 maxTime 需要大于0
  /// - V2TXLIVE_ERROR_REFUSED: 播放器处于播放状态，不支持修改缓存策略
  Future<V2TXLiveCode> setCacheParams(double minTime, double maxTime) async {
    var result = await _channel.invokeMethod('setCacheParams', {
      "minTime": minTime.toString(),
      "maxTime": maxTime.toString()
    });
    return _liveCodeWithResult(result);
  }

  /// 启用播放音量大小提示。
  ///
  /// 开启后可以在 {@link V2TXLivePlayerObserver#onPlayoutVolumeUpdate(V2TXLivePlayer, int)} 回调中获取到 SDK 对音量大小值的评估。
  ///
  /// intervalMs 决定了 onPlayoutVolumeUpdate 回调的触发间隔，单位为ms，最小间隔为100ms，如果小于等于0则会关闭回调，建议设置为300ms；【默认值】：0，不开启
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> enableVolumeEvaluation(int intervalMs) async {
    var result = await _channel.invokeMethod(
        'enableVolumeEvaluation', {"intervalMs": intervalMs});
    return _liveCodeWithResult(result);
  }

  /// 截取播放过程中的视频画面。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_REFUSED: 播放器处于停止状态，不允许调用截图操作
  Future<V2TXLiveCode> snapshot() async {
    var result = await _channel.invokeMethod('snapshot', {});
    return _liveCodeWithResult(result);
  }

  /// 开启/关闭对视频帧的监听回调。
  ///
  /// SDK 在您开启次此开关后将不再渲染视频画面，您可以通过 V2TXLivePlayerObserver 获得视频帧，并执行自定义的渲染逻辑。
  ///
  /// enable      是否开启自定义渲染。【默认值】：false
  /// pixelFormat 自定义渲染回调的视频像素格式 {@link V2TXLiveDef.V2TXLivePixelFormat}。
  /// bufferType  自定义渲染回调的视频数据格式 {@link V2TXLiveDef.V2TXLiveBufferType}。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_NOT_SUPPORTED: 像素格式或者数据格式不支持
  Future<V2TXLiveCode> enableObserveVideoFrame(
      bool enable, int pixelFormat, int bufferType) async {
    var result = await _channel.invokeMethod('enableObserveVideoFrame', {
      "enable": enable,
      "pixelFormat": pixelFormat,
      "bufferType": bufferType
    });
    return _liveCodeWithResult(result);
  }

  /// 开启接收 SEI 消息
  ///
  /// enable      true: 开启接收 SEI 消息; false: 关闭接收 SEI 消息。【默认值】: false
  /// payloadType 指定接收 SEI 消息的 payloadType，支持 5、242，请与发送端的 payloadType 保持一致。
  ///
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  Future<V2TXLiveCode> enableReceiveSeiMessage(
      bool enable, int payloadType) async {
    var result = await _channel.invokeMethod('enableReceiveSeiMessage',
        {"enable": enable, "payloadType": payloadType});
    return _liveCodeWithResult(result);
  }

  /// 是否显示播放器状态信息的调试浮层。
  ///
  /// isShow 是否显示。【默认值】：false
  Future<void> showDebugView(bool isShow) async {
    await _channel.invokeMethod('showDebugView', {"isShow": isShow});
  }

  /// 调用 V2TXLivePlayer 的高级 API 接口。
  ///
  /// @param key   高级 API 对应的 key。
  /// @param value 调用 key 所对应的高级 API 时，需要的参数 jsonString。
  /// @return 返回值 {@link V2TXLiveCode}
  /// - V2TXLIVE_OK: 成功
  /// - V2TXLIVE_ERROR_INVALID_PARAMETER: 操作失败，key 不允许为 null
  /// @note 该接口用于调用一些高级功能。
  Future<V2TXLiveCode> setProperty(String key, Object value) async {
    var result = await _channel.invokeMethod(
        'setProperty', {"key": key, "value": value});
    return _liveCodeWithResult(result);
  }
}
